////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Implementation of the CommandParser class.

*/

////////////////////////////////////////////////////////////////////////////////
#include "CommandParser.h"
#include <iostream>

CommandParser::CommandParser()
{

}

CommandParser::~CommandParser()
{

}

Command CommandParser::RetrieveCommand(const std::string& arguments)
{
    std::cout << "Asked to 'retrieve' the command " << arguments << std::endl;
    return Command::Unknown;
}
