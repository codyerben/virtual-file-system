////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Definition of the ICommandParser interface.

*/

////////////////////////////////////////////////////////////////////////////////
#ifndef ICOMMAND_PARSER_H
#define ICOMMAND_PARSER_H

#include <string>
#include "Command.h"

class ICommandParser
{
    public:

        virtual Command RetrieveCommand(const std::string& arguments) = 0;
};

#endif
