////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Entrypoint for the Virtual File System application.

*/

////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <chrono>
#include <sstream>
#include <signal.h>

#include "CommandParser.h"

void DisplayUnsupportedConfigurationMessage()
{
    const char* UNSUPPORTED_CONFIGURATION = 
        "Currently, this application does not support command line arguments.  Please try again without any arguments";

    std::cout << UNSUPPORTED_CONFIGURATION << std::endl;
}

void DisplayStartupMessage()
{
    const char *CODY_INITIALS = "RCE";
    const char *APPLICATION_GREETING = "Welcome to the Virtual File System application.";
    const char *APPLICATION_GREETING_FOOTER = "  designed and developed by: ";

    std::cout << APPLICATION_GREETING << std::endl;
    std::cout << APPLICATION_GREETING_FOOTER << CODY_INITIALS << std::endl;
}

std::string getTerminalPrefix()
{
    const char *TERMINAL_HEADER = "RCE-VFS ";
    const char *TERMINAL_TIME_FORMAT = "%H:%M";
    const int BUFFER_STRING_SIZE = 20;

    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::string currentTime(BUFFER_STRING_SIZE, '\0');
    std::strftime(&currentTime[0], currentTime.size(), TERMINAL_TIME_FORMAT, std::localtime(&now));

    std::ostringstream stringStream;
    stringStream << TERMINAL_HEADER << currentTime << " $ ";
    
    return stringStream.str();
}

void ProcessUserInput()
{
    CommandParser commandParser;

    std::string consoleInput;
    while (std::getline(std::cin, consoleInput))
    {
        // ! Get the command from the user and do some work.

        commandParser.RetrieveCommand(consoleInput);

        std::cout << getTerminalPrefix();
    }
}

void PersistFileSystem()
{

}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        DisplayUnsupportedConfigurationMessage();
        return 1;
    }
    else
    {
        DisplayStartupMessage();
        std::cout << getTerminalPrefix();

        ProcessUserInput();

        PersistFileSystem();

        return 0;
    }
}
