# README #

This is a learning project application utilizing:
* Modern C++ (C++ 11)
* Google Test and Google Mock
* VSCode for an IDE:
 * Extensions:
  * GoogleTest Adapter
  * Coverage Gutters
  * Microsoft CPP Support (CPPTools, C/C++)
 * My Extensions:
  * GitLens
  * Peacock
  * Live Share

# The Project #

Build a virtual file system utilizing things such as Test Driven Development, the agreed upcon coding guidelines, and so forth.

# Coverage related commands

* Command to generate LCOV info file which will be used by coverage gutter to show coverage

    lcov --directory build/vfs/CMakeFiles/vfs.dir/src/. --capture --output-file lcov.info
 
* Command to generate html coverage report from coverage info file

    genhtml --output-directory coverage --demangle-cpp --num-spaces 4 --sort --title "Virtual File System Test Coverage" --function-coverage --branch-coverage --legend lcov.info
