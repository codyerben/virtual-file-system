cmake_minimum_required(VERSION 3.0)

set(This vfs_test)
project(${This} C CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(GCC_COVERAGE_COMPILE_FLAGS "-g -O0 -coverage -fprofile-arcs -ftest-coverage")
set(GCC_COVERAGE_LINK_FLAGS "-coverage -lgcov")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}")

enable_testing()

include_directories(
    "../vfs/inc"
)

# Some recommendations on NOT using file globbing to grab all cpp files.  So far, it seems
# ok, so rolling with it until we get more experience.

file(GLOB UNIT_TEST_SOURCES
    "src/*.cpp"
)

add_executable(${This} ${UNIT_TEST_SOURCES})

target_link_libraries(${This} PUBLIC
    vfs	
    gtest_main
    gmock_main
    gtest
    gmock
    pthread
)
