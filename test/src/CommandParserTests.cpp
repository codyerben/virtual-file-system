////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Unit tests for the CommandParser class.

*/

////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "CommandParser.h"

using namespace testing;

TEST(CommandParserTests, CommandParserRetrieveCommandValidCommand)
{
    CommandParser parser;
    EXPECT_EQ(Command::Unknown, parser.RetrieveCommand(""));
}
